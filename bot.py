import discord
import os
from dotenv import load_dotenv
from discord.ext import commands

# Load api token
load_dotenv()
TOKEN = os.getenv('DISCORD_TOKEN')

client = commands.Bot(command_prefix = '!')

# Load extension
@client.command()
async def load(ctx, extension):
  client.load_extension(f'cogs.{extension}')

# Unload extension
@client.command()
async def unload(ctx, extension):
  client.unload_extension(f'cogs.{extension}')

# Load all extensions in cogs directory
for filename in os.listdir('./cogs'):
  # if it's a py file
  if filename.endswith('.py'):
    # load it (without the '.py')
    client.load_extension(f'cogs.{filename[:-3]}')

client.run(TOKEN)