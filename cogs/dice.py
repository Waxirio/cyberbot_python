import discord
from discord.ext import commands
import random

class DiceError(commands.CommandError):
  """Custom Exception class for dice error."""

# first example command cog
class Dice(commands.Cog):
  # initialisation function
  def __init__(self, client):
    self.client = client

  @commands.Cog.listener()
  async def on_ready(self):
    print("dice loaded")
  
  @commands.command(name="roll", aliases=['throw'])
  """ Lancé de dés """
  async def roll(self, ctx, dice_param: str):
    if('d' in dice_param):
      try:
        dice_split = dice_param.split('d')
        if len(dice_split) == 2 :
          nb_dice = int(dice_split[0])
          face_dice = int(dice_split[1])
          all_throws = ""
          final_res = 0
          for i in range(0, nb_dice):
            current_roll = random.randint(1, face_dice)
            final_res += current_roll
            all_throws += str(current_roll) + " "
          embed=discord.Embed(title="Résultat du lancé", color=0xe13737)
          embed.set_author(name=f'{ctx.author.name}', icon_url=f'{ctx.message.author.avatar_url}')
          embed.add_field(name="Lancés :", value=f'{all_throws}', inline=False)
          embed.add_field(name="Final :", value=f'{final_res}', inline=True)
          embed.set_footer(text="Lancé de dés")
          await ctx.send(embed=embed)
      except:
        raise DiceError('No dice given')
    else:
      await ctx.send("`usage: roll [Nombre de dés]d[Nombre de face]`", delete_after=10)

# setup function
def setup(client):
  client.add_cog(Dice(client))