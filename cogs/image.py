import discord
from discord.ext import commands
import os
import random

# first example command cog
class Image(commands.Cog):
  """ Classe d'image pour l'affichage d'ellen page """
  # initialisation function
  def __init__(self, client):
    self.client = client

  @commands.Cog.listener()
  async def on_ready(self):
    print("image loaded")
  
  @commands.command(name="ellen", aliases=["je t'aime ellen !", "ellen page"])
  async def ellen(self, ctx):
    """ Chargement des images de ellen """
    random_file = random.choice(os.listdir("./ellen/"))
    await ctx.send(file=discord.File('./ellen/'+random_file, 'ellen.png'))

  @commands.command(name="elliot", aliases=["elliot page"])
  async def elliot(self, ctx):
    """ Chargement de l'image d'elliot """
    with open('./elliot/elliot.png', 'rb') as fp:
      await ctx.send(file=discord.File(fp, 'elliot.png'))

# setup function
def setup(client):
  client.add_cog(Image(client))