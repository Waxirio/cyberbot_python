import discord
from discord.ext import commands

# first example command cog
class Example(commands.Cog):
  # initialisation function
  def __init__(self, client):
    self.client = client

  @commands.Cog.listener()
  async def on_ready(self):
    """ Message lors du chargement du cog """
    print("ping loaded")
  
  @commands.command()
  async def cooki(self, ctx):
    """ Ping pour envoyer des cookies """
    await ctx.send(':cookie:')

# setup function
def setup(client):
  client.add_cog(Example(client))